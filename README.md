# backstage-lizard-poc

a mocked demo of Backstage using Lizard as an example

documentation
- https://backstage.pages.gitlab.tech.orange/backstage-knowhow/know-how/modeling/#lizard

content
- `catalog-model-lizard`: static model (ie. not fetched from the gitlab plugin)
